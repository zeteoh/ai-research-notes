# AI Research Notes

Notes about research papers related to model compression methods (quantization, pruning, binarized, etc), neural architecture search, federated learning and weakly/self supervised learning.
# History

* [21/01/07] μNAS: Constrained Neural Architecture Search for Microcontrollers
    * Notes: https://gitlab.com/zeteoh/ai-research-notes/-/issues/4
    * Paper's link: https://arxiv.org/abs/2010.14246
* [20/08/20] DeepGMR: Learning Latent Gaussian Mixture Models for Registration
    * Notes: https://gitlab.com/zeteoh/ai-research-notes/-/issues/3
    * Paper's link: https://arxiv.org/abs/2008.09088v1
* [20/08/03] Predicting What You Already Know Helps: Provable Self-Supervised Learning
    * Notes: https://gitlab.com/zeteoh/ai-research-notes/-/issues/2
    * Paper's link: https://arxiv.org/abs/2008.01064v1
* [20/08/19] NASCaps: A Framework for Neural Architecture Search to Optimize the Accuracy and Hardware Efficiency of Convolutional Capsule Networks 
    * Notes: https://gitlab.com/zeteoh/ai-research-notes/-/issues/1
    * Paper's link: https://arxiv.org/abs/2008.08476v1


